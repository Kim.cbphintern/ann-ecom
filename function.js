//FOR BROADER SCOPE
var total;
var nbtotal;
var assum;
var counter = 1;
var dif;
var asqty;
var add;
var quantity;
var nbqty;
var val;

function test1(){
     quantity = document.getElementById("qty1").value;
    if (quantity % 3 === 0){ //check if divisible by 3
        var divide = quantity/3;
        var difference = quantity - divide;
        total = difference * 50;
        console.log(total);
        document.getElementById('total').innerHTML = "$"+total;
    }else{        
        total = quantity * 50;
        document.getElementById('total').innerHTML = "$"+total;
        console.log(total);
    }
    //Check NaN
    if(quantity.length < 1){
        document.getElementById('total').innerHTML = '';
    }
}

function test2(){
    nbqty = document.getElementById('qty2').value;
    if (nbqty > 3){
        nbtotal = nbqty * 55;
        document.getElementById('total1').innerHTML = "$"+nbtotal;
    }else{
        nbtotal = nbqty * 70;
        document.getElementById('total1').innerHTML = "$"+nbtotal;
    }
     //Check NaN
     if(nbqty.length < 1){
        document.getElementById('total1').innerHTML = '';
    }
}

function test3(){
        asqty = document.getElementById("qty3").value; 
        assum = asqty * 60;
        document.getElementById('total2').innerHTML = "$"+assum;
        document.getElementById('giveaway').innerHTML = asqty+ " x havaianas"
      //Check NaN
      if(asqty.length < 1){
        document.getElementById('total2').innerHTML = '';
        document.getElementById('giveaway').innerHTML = '';
    }
}
function test4(){
    val=document.getElementById('qty4').value;
    add = val * 20;
    document.getElementById('total3').innerHTML = "$ "+add;
    //Check NaN
    if(val.length < 1){
        document.getElementById('total3').innerHTML = '';
    }
}
//ENABLE TEXTBOX
function enabletxtbox(bEnable, textBoxID)
{
     document.getElementById(textBoxID).disabled = !bEnable;
}
//ACCEPT NUM ONLY 
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
        return false;
    }
    return true;
}
//COMPUTE AND LIST
function compute(){
    //CHANGE THE VALUE OF EMPTY TXTBOX TO 0 TO SUM UP ALL
    if (total == undefined){
        total = '0'; 
    }               
    if (nbtotal == undefined){
        nbtotal = '0';                
    }       
    if (assum == undefined){
        assum = '0';
    }
    if (add == undefined){
        add = '0';
    }
    var sum = parseInt(total) + parseInt(nbtotal) + parseInt(assum) + parseInt(add);
    console.log(total+" "+nbtotal+" "+" "+assum+" "+add);
    var promo =document.getElementById('code').value;
    var convert = promo.toUpperCase();
    
    var checks = document.getElementsByClassName('checks');
    var qty = document.getElementsByClassName('txtbox');
    var str = '';
    var num = '';
    for (i=0;i<4;i++){
      if(qty[i].disabled != true){
        if (checks[i].checked === true){
        num = qty[i].value + " ";
        str += num+" x "+checks[i].value + "<br>";
        }
    }
}  
   // ADDING NEW ROWS IN THE TABLE
    var table = document.getElementsByTagName('table')[0];

    var newRow = table.insertRow(table.rows.length);

    var cell1 = newRow.insertCell(0);
    var cell2 = newRow.insertCell(1);
    var cell3 = newRow.insertCell(2);
    var cell4 = newRow.insertCell(3);
    var cell5 = newRow.insertCell(4);

    cell1.innerHTML = counter++;
    cell2.innerHTML = str;
    if (convert == "SNEAKERH3AD"){
        var discount = sum * .10;
        var sub = sum - discount;
        cell3.innerHTML = "$ "+sub;
    }else{
        cell3.innerHTML = "$ "+sum;
    }
   
    cell4.innerHTML = str;
    console.log("as"+asqty);
    if(asqty != undefined){
    cell5.innerHTML = asqty+" x havaianas"
    }else{
        cell5.innerHTML = '';
    }
    asqty = undefined;
     //CLEAR DATA AFTER CLICKING THE BTNN
     document.getElementById('qty1').value='';
     document.getElementById('qty2').value='';
     document.getElementById('qty3').value='';
     document.getElementById('qty4').value='';
     document.getElementById('code').value='';
     document.getElementById('total').innerHTML='';
     document.getElementById('total1').innerHTML='';
     document.getElementById('total2').innerHTML='';
     document.getElementById('total3').innerHTML='';
     document.getElementById('giveaway').innerHTML = '';
     for (i=0;i<4;i++){
          checks[i].checked = false;
          qty[i].disabled = true;
  }  
     //BRING BACK THE VALUE TO 0
     total=0;
     nbtotal=0;
     assum=0;
     add=0;
}